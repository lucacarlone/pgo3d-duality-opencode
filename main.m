close all
clc
clear all

%% User defined settings
solveSDP  = 1; % not needed for verification 2
sigmaT = 0.1; % translation noise
sigmaR = 0.1; % rotation noise

%% Scenario to run
% scenario = 'grid3D'; % this is now a large scenario
% scenario = 'smallGrid3D'; % this is the one we were testing earlier
 scenario = 'randomGrid3D'; % generate a new one at each run
% scenario = 'loop3D';
% scenario = 'randomLoop3D'; % generate a new one at each run
% scenario = 'randomTorus3D'; % generate a new one at each run
% scenario = 'cubicle'; 
% scenario = 'rim'; 
% scenario = 'parking-garage'; 
% scenario = 'sphere2500vertigo'; 
% scenario = 'sphere_bignoise_vertex3'; 
% scenario = 'toy3D'; % noiseless
% scenario = 'toyExample'; % noiseless

%% Fixed settings
addpath(genpath('./lib'));
cvxQuiet = true;
initialization = 'odometry'; 
rotEstApproach = 'X'; % this parameter does not apply to this case
orderEdgesFlag = 0;
prinfFig = 0;
lgray = [0.8, 0.8, 0.8];
dgreen = [0 .5 0];
brown = [0.8 0.5 0];
orange = [1 0.6 0];
colorOdom = 'r';
colorInit = 'b';
colorRefined = 'k';
inputFile = horzcat('./datasets/',scenario,'.g2o');

%% re-generate random scenarios
switch scenario
    case 'randomGrid3D'
        disp('- writing randomGrid3D')
        side = 3; % number of nodes on the side of the cube
        probLC = 0.1; % probability of loop closure
        writeGridDataset3D(side, side, side, probLC, sigmaT, sigmaR, inputFile);
    case 'randomLoop3D'
        disp('- writing randomLoop3D')
        nrNodes = 10; % number of nodes
        r = 20; % radius of the loop
        writeLoopDataset3D(nrNodes, r, sigmaT, sigmaR, inputFile);
    case 'randomTorus3D'
        disp('- writing randomTorus3D')
        nrNodes = 20; % number of nodes
        coils = 10;
        probLC = 0.1;
        writeTorusDataset3D(nrNodes, coils, probLC, sigmaT, sigmaR, inputFile);
    otherwise
        disp('loading scenario from file')
end

%% Read dataset file, plot initial guess
[measurements, edges_id, odomPoses] = readG2oDataset3D(inputFile, orderEdgesFlag);
plotPose3Graph(odomPoses, edges_id, colorOdom, horzcat('OdomGuess-',scenario), prinfFig);
nrNodes = length(odomPoses);
m = length(measurements.between); % number of edges
posesInitialization = odomPoses;

%% evaluate cost and anchor (set first pose to identity)
[halfCostPose3DAngGTSAM_befAnc,~,~,~,costPose3DChordInit_befAnc] = evaluateCostPose3D(posesInitialization, measurements);
posesInitialization = anchorEstimate(posesInitialization);
[halfCostPose3DAngInit,~,~,~,costPose3DChordInit] = evaluateCostPose3D(posesInitialization, measurements);
plotPose3Graph(posesInitialization, edges_id, colorInit, horzcat('init-', initialization,'-',scenario,'-',rotEstApproach), prinfFig);
costPose3DAngInit = 2*halfCostPose3DAngInit;
% sanity checks
checkEqualWithTol(halfCostPose3DAngGTSAM_befAnc,halfCostPose3DAngInit, 1e-3);
checkEqualWithTol(costPose3DChordInit_befAnc,costPose3DChordInit, 1e-3);
fprintf('## %s(%s) Cost: angular: %.10f, chordal: %.10f \n', initialization, rotEstApproach, costPose3DAngInit, costPose3DChordInit);
disp('====================================================================')

%% Use duality for verification
disp('=================  SOLUTION VERIFICATION (POSES) ===================')
disp('====================================================================')
disp('--> Solve primal problem with iterative method (GN)')
[poseEstimate, iterPose3D] = refinePose3D(measurements, posesInitialization);
plotPose3Graph(poseEstimate, edges_id, colorRefined, horzcat('refinedPoses-', initialization,'-',scenario,'-',rotEstApproach), prinfFig);
[halfcostPose3DAng,~,~,~,costPose3DChord] = evaluateCostPose3D(poseEstimate, measurements);
costPose3DAng = 2*halfcostPose3DAng; % halfcostPose3DAng = cost returned by GTSAM
disp('====================================================================')
fprintf('## Primal cost: angular: %.10f, chordal: %.10f \n', costPose3DAng, costPose3DChord);
disp('====================================================================')

%% Verify objective using the dual problem
% Build matrix M
[A_pose, b_pose, Afull_pose, M_pose] = buildLinearModelPose3D(measurements, nrNodes);
checkLinearModelPose3D(A_pose, b_pose, M_pose, poseEstimate, costPose3DChord);
% VERSION: anchored
n = nrNodes-1;
AtA_pose = A_pose'*A_pose;
Atb_pose = A_pose'*b_pose;
btb_pose = b_pose'*b_pose;
H_pose = [AtA_pose,  -Atb_pose; -Atb_pose',  btb_pose];
relTol = 1e-2;

%% Verification V1
if solveSDP
  disp('**********************  VERIFICATION V1 (solving SDP, SDP_Pose3D_v20)  ******************************')
  [dstar, gammaAnc, lambdaMatAnc, verifyTime.ver1Time.SDPTime, primalViaDualSol, verifyTime.ver1Time.primalViaDualTime] = ...
      SDP_Pose3D_v20(H_pose, measurements, costPose3DChord, cvxQuiet);
  fstarViaDual = primalViaDualSol.costPose3Ddual_v2;

  %% Verification V1
  dgap1 = costPose3DChord - dstar;
  fprintf('## Verification V1: duality gap: %.10f \n', dgap1);
else
  dstar = 0;
  fstarViaDual = 0; 
  verifyTime.ver1Time.SDPTime = 0;
  verifyTime.ver1Time.primalViaDualTime = 0;
end

%% Verification V2
  disp('**********************  VERIFICATION V2  ******************************')
homPoseVectPrimal = poses3D2Vect(poseEstimate);
% version 2, corresponding to problem SDP_Pose3D_v20
[dhatViaPrimal, normLinSysRes, verifyTime.ver2Time, minEigHlamHat, Hlam] = optimalityVerification2_v2(H_pose, homPoseVectPrimal, n, costPose3DChord);
