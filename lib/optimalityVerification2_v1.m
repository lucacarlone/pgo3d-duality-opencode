function [dhatViaPrimal, normLinSysRes, optTimeLin] = optimalityVerification2_v1(H_pose, homPoseVectPrimal, n, costPose3DChord)
% COMMENTS:
% H_pose in Real{(12n+1) X (12n+1)}
%
% Now, we know that a primal-dual optimal pair (x,lam) satisfies:
%
% [ Hpose - Delta(lam) ] x = 0  (x in the null space of Hpose(lam) = Hpose + Delta(lam))
%
% Since Hpose has the structure 
% Hpose = [Htt  HtR  Hty
%          HRt  HRR  HRy
%          Hyt  HyR  Hyy]
%
% and Delta only affects the blocks HRR and Hyy, then x already 
% has to satisfy: [Htt  HtR  Hty] x = 0
% or equivalently, the first 3n entries of (bx = H_pose * x) must be zero 
% (this is the translation part, tranResidue in the code)
%
% Now the remaining part of the system depends on lam
% [HRt  HRR  HRy] x = 0    <=> bx(3n+1:end) = Alam * lam
% [Hyt  HyR  Hyy]
% 
% Given a candidate x, we can compute lam that makes bx(3n+1:end) = Alam * lam = 0
%

disp('====================================================================')
ZZZ = spalloc( 3*n, 3*n, 0); % matrix of zeros
% we assume that the candidate solution is in the form [x 1]
checkEqualWithTol(homPoseVectPrimal(end),1,1e-5);

Hx = H_pose * homPoseVectPrimal; % Hpose(lam) x = 0 <=> Hpose*x + Delta(lam)*x = 0 <=> bx = -Delta(lam)*x 
tranResidue  = Hx(1:3*n);
Hx_rot_y = Hx(3*n+1:end);

%% Build coefficient matrix for lambda
homRotVectPrimal = homPoseVectPrimal(3*n+1:end);  % x_rot_y
A_rot_y = spalloc(9*n+1, 6*n+1, 6*(9*n) + 6*n+1);
Z3 = zeros(3,1);
sel = [1 0 0, 1 0, 1];
for i=1:n
  rowInd9 = blockToMatIndices(i, 9);
  x1 = homRotVectPrimal(rowInd9(1:3));
  x2 = homRotVectPrimal(rowInd9(4:6));
  x3 = homRotVectPrimal(rowInd9(7:9));

  colInd6 = blockToMatIndices(i, 6);
  A_rot_y(rowInd9,colInd6) = [x1 x2 x3 Z3 Z3 Z3;
                              Z3 x1 Z3 x2 x3 Z3;
                              Z3 Z3 x1 Z3 x2 x3];
  A_rot_y(9*n+1,colInd6) = -sel; % this is for the last condition: sum lambda (diag) - gamma
end
A_rot_y(9*n+1, 6*n+1) = +1; % coefficient of gamma
linSolTime = tic;
lam_y = A_rot_y \ Hx_rot_y;
optTimeLin = toc(linSolTime);

dhatViaPrimal = lam_y(end);
gap2 = costPose3DChord - dhatViaPrimal;
if gap2 > 0.01
  cprintf('_red','## VERIFICATION2, v1: %.10f <= cost: %.10f (CPU time: %f)\n', dhatViaPrimal, costPose3DChord, optTimeLin);
else
  fprintf('## VERIFICATION2, v1: %.10f <= cost: %.10f (CPU time: %f)\n', dhatViaPrimal, costPose3DChord, optTimeLin);
end

%% Compute residul errors, to check if we actually solved the linear system
rotResidue = A_rot_y * lam_y - Hx_rot_y;
normTranRes = norm(tranResidue);
normRotRes  = norm(rotResidue);
normLinSysRes = norm([tranResidue; rotResidue]);
fprintf('== VERIFICATION2, v1: normTranRes = %f, normRotRes = %f, normLinSysRes = %f, eigHpose = %f \n', normTranRes, normRotRes, normLinSysRes, eigs(H_pose, 1, 'SM'))
if normLinSysRes > 1e-2
  warning('normLinSysRes is NOT zero')
end

%% Check that H(lambda) is not negative definite
lambdaMatHat = spalloc(3*n, 3*n, 3*(3*n));
for i=1:n
  colInd6 = blockToMatIndices(i, 6);
  lam_i = lam_y(colInd6);
  ind3 = blockToMatIndices(i, 3);
  lambdaMatHat(ind3,ind3) = [lam_i(1) lam_i(2) lam_i(3); lam_i(2) lam_i(4) lam_i(5); lam_i(3) lam_i(5) lam_i(6)];
end
Hlam = H_pose + blkdiag( ZZZ, -kron(lambdaMatHat, eye(3)) , trace(lambdaMatHat)-lam_y(end) );
% Wrong, this is the smallest *magnitude* eigenvalue
%minEigHlamHat = eigs(Hlam, 1, 'SM');
minEigHlamHat = computeSmallestEig(Hlam);

fprintf('## VERIFICATION2, v1: (1) is dhat: %.10f == cost: %.10f, (2) is normLinSysRes: %f small?, (3) is lam_min(Hlam): %f >=0, (4) is min(lambdaDiag): %f >= 0  (CPU time: %f)\n', ...
  dhatViaPrimal, costPose3DChord, normLinSysRes, minEigHlamHat, full(min(diag(lambdaMatHat))), optTimeLin);

% Sanity check for solution of the linear system
checkEqualWithTol(normLinSysRes, norm(Hlam * homPoseVectPrimal), 1e-2);
