disp('=====================  SOLUTION VERIFICATION (ROTATIONS) ======================')
%% Solve primal problem
disp('--> Refining angle estimate')
measurements.edges_id = edges_id;
rotationEstimate = refineRot3D(measurements, posesInitialization);
disp('--> Evaluating cost')
rotCost3D = evaluateFrobeniusCostRot3D(measurements, rotationEstimate);

%% Verify objective using the dual problem
disp('--> Computing lower bound on optimal objective via SDP')
% Build matrix Q
[A, b, Afull, Q] = buildLinearModelRot3D(measurements, nrNodes);
checkLinearModelRot3D(A, b, Q, rotationEstimate, rotCost3D);

if solveSDP
  % VERSION 1 (produces a loose bound): Solve SDP as in Luca's notes
  % SDP_Rot3D_v10
  % VERSION 1.1 (simplified anchoring w/o right hand side, loose bound)
  % SDP_Rot3D_v11
  
  %% VERSION 2 (produces a very good bound): Solve SDP with fixed anchor node
  SDP_Rot3D_v20
  
  % VERSION 2.1 (produces a good bound - slightly worse than v2): Solve SDP with fixed anchor node, but nicer homogenization
  % SDP_Rot3D_v21
  % VERSION 2.2 (produces a good bound - slightly worse than v2): Solve SDP with fixed anchor node, and schur complement
  % SDP_Rot3D_v22
end