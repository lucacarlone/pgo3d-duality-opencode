function [poseEstimates, iter] = refinePose3D(measurements, poseEstimates)
% poseEstimates(i).R contains the i-th rotation
% poseEstimates(i).t contains the i-th translation
isDebug = 1;

edges_id = measurements.edges_id;
m = size(edges_id,1);
nrNodes = length(poseEstimates);
n = nrNodes-1;
Jtt = spalloc(3*m, 3*nrNodes, 2*3*m); % 2 nnz elements per row
Jtr = spalloc(3*m, 3*nrNodes, 3*3*m); % 3 nnz elements per row
Jrr = spalloc(9*m, 3*nrNodes, 6*9*m); % 6 nonzero per row
ft = sparse(zeros(3*m,1));
fr = sparse(zeros(9*m,1));
cost = 1e+20;
I3 = sparse(eye(3));
ZZ = spalloc(9*m, 3*(nrNodes-1),0);

%% matrix such that Si * u = ith column of skew(u)
S1 = sparse([0 0 0 ; 0 0 1; 0 -1 0]);
S2 = sparse([0 0 -1; 0 0 0; 1  0 0]);
S3 = sparse([0 1  0;-1 0 0; 0  0 0]);

%% First order expansion of the cost || Ri _Rij - Rj ||_F = 
%  || _Ri (I + S(thi)) _Rij - _Rj (I + S(thj)) ||_F
%  || _Ri _Rij + _Ri S(thi) _Rij - _Rj - _Rj S(thj) ||_F (rearranging)
%  || (_Ri _Rij - _Rj) + _Ri _Rij S(_Rij' thi) - _Rj S(thj) ||_F (vectorizing by columns)
%  || _rr + [_Ri _Rij S1; _Ri _Rij S2; _Ri _Rij S3] _Rij' thi - [_Rj S1; _Rj  S2; _Rj S3] thj ||_F
%% First order expansion of the cost || tj - ti - Ri Deltaij || = 
%  || tj + dtj - ti -dti - _Ri (I + S(thi) ) Deltaij || = 
%  || (tj - ti - _Ri Deltaij) + dtj - dti - _Ri S(thi) Deltaij || = 
%  || _rt + dtj - dti + _Ri S(Deltaij) thi || = 
for iter=1:10
  %% linerizeRot();
  costPrec = cost;
  for k=1:m
    id1 = edges_id(k,1);
    id2 = edges_id(k,2);
    ti = poseEstimates(id1).t;            Ri = poseEstimates(id1).R;
    tj = poseEstimates(id2).t;            Rj = poseEstimates(id2).R;
    Deltaij = measurements.between(k).t;  Rij = measurements.between(k).R;
    Omegaij = measurements.between(k).Info;
    Omegaijtran = Omegaij(1:3,1:3);
    Omegaijrot = Omegaij(4:6,4:6);
    if norm( Omegaijtran - eye(3)*Omegaijtran(1,1) ,'inf')>1e-5 || norm( Omegaijrot - eye(3)*Omegaijrot(1,1) ,'inf')>1e-5
      warning('Frobenius norm requires isotropic covariances')
    end
    sqrtInfoTran = sqrt(Omegaijtran(1,1));
    sqrtInfoRot = sqrt(Omegaijrot(1,1)/2); % 2 is because cost if |tran|^2 + 0.5 |rot|^2
    
    %% Fill in translation part
    rowInd = blockToMatIndices(k,3);
    ft(rowInd) = sqrtInfoTran * (tj - ti - Ri * Deltaij); % residual vector
    
    colInd1 = blockToMatIndices(id1,3);
    Jtt(rowInd,colInd1) = sqrtInfoTran * (-I3);
    Jtr(rowInd,colInd1) = sqrtInfoTran * Ri * skew(Deltaij);
    
    colInd2 = blockToMatIndices(id2,3);
    Jtt(rowInd,colInd2) = sqrtInfoTran * (I3);
    
    %% Fill in rotation part
    rowInd = blockToMatIndices(k,9);
    Ri_Rij = Ri * Rij;
    fr(rowInd) = sqrtInfoRot * vec( Ri_Rij - Rj);
    
    colInd1 = blockToMatIndices(id1,3);
    Jrr(rowInd,colInd1) = sqrtInfoRot * [Ri_Rij*S1 ; Ri_Rij*S2; Ri_Rij*S3] * Rij';
    
    colInd2 = blockToMatIndices(id2,3);
    Jrr(rowInd,colInd2) = - sqrtInfoRot * [Rj*S1 ; Rj*S2; Rj*S3];
  end

  %% solvePoses
  fhat = [ft; fr];
  Jobs = [ Jtt(:,4:end)  Jtr(:,4:end); ZZ  Jrr(:,4:end)]; % we discard anchor 
  corrPoseEst = - (Jobs'*Jobs) \ (Jobs' * fhat); % minus because of fhat - translation, then rotations
  cost = norm(fhat)^2;
  relCostChange = abs((cost - costPrec)/costPrec);
  if (isDebug) fprintf('Iter: %d, Current cost: %f, norm of the correction: %f, relative decrease: %f \n',iter,cost,norm(corrPoseEst),relCostChange); end
  
  %% updatePoses
  corrTest = corrPoseEst(1:3*n);
  corrRotEst = corrPoseEst(3*n+1:end);
  for i=2:nrNodes
    rowInd = blockToMatIndices(i-1,3);
    
    corrThEst_i = corrRotEst(rowInd); % vector 3
    poseEstimates(i).R = poseEstimates(i).R * rot_exp(I3,rot_hat(I3,corrThEst_i));
    
    corrTest_i = corrTest(rowInd); % vector 3
    poseEstimates(i).t = poseEstimates(i).t + corrTest_i;
  end
  
  %% check stopping conditions
  if norm(corrPoseEst) < 1e-6 || relCostChange < 1e-8
    break;
  end
end

end