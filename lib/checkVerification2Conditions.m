function isOptimal = checkVerification2Conditions(costPose3DChord,dhatViaPrimal, normLinSysRes, minEigHlamHat)

if checkEqualWithTol(costPose3DChord,dhatViaPrimal,1e-1,'noError') && ... % d(lamHat) = fhat
    normLinSysRes < 1 && ... % system is solvable
    minEigHlamHat > -0.5 % eigenvalue is positive
  
    isOptimal = 1;
else
    isOptimal = 0;
end
    
    