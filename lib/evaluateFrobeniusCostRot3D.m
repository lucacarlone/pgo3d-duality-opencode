function rotCost3D = evaluateFrobeniousCostRot3D(measurements, rotationEstimate)
% Evaluate the following rotation cost:
%
% sum_ij 1/2 ( omega_ij^2 * norm( Ri Rij - Rj , 'fro') ) =
%
% Luca Carlone
% Georgia Institute of Technology
% Nov 2014

edges_id = measurements.edges_id;
m = size(edges_id,1);

rotCost3D = 0;
for k=1:m 
  id1 = edges_id(k,1);
  id2 = edges_id(k,2);
  Ri = rotationEstimate(id1).R;
  Rj = rotationEstimate(id2).R;
  Rij = measurements.between(k).R;
  
  Omegaij = measurements.between(k).Info;
  OmegaijR = Omegaij(1:3,1:3);
  if norm(OmegaijR - OmegaijR(1,1) * eye(3,3),'inf') > 1e-6 % check that the matrix is diagonal & isotropic 
    error('Rotation covariance should be diagonal and isotropic');
  end
 
  inf_th = sqrt ( OmegaijR(1,1) ); % inverse of std for rotation measurements
  X = inf_th * ( Ri * Rij - Rj );
  rotCost3D = rotCost3D + norm(X,'fro')^2;
end