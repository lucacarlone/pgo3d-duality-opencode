clear all
close all
clc

for i=1:5
   A(:,:,i) = ones(4,4); 
   F{i} = ones(4,4); 
end
lam = [1:5];

%A(:,:,:) .* lam

% sum(A(:,:,:),3)
% 
% A(:,:,:) .* lam(:,:,:)

%bsxfun(@times,A,lam);

% B = [1 2; 3 4];
% A = 1:5;
% D = bsxfun(@times,B,reshape(A,1,1,numel(A)));

sum(bsxfun(@times,A,reshape(lam,1,1,numel(lam))),3)
