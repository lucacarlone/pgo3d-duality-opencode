function [tj, Rj] = poseAdd3D(p_i , delta_ij)   
% pose_add(p_i, p_ij) = p_j. 3D Pose composition. 
% The second is the delta pose to be added (to the right)
% of the first pose
%
% Author: Luca Carlone
% Date: July 2014

Rj = p_i.R * delta_ij.R;
tj = p_i.t + p_i.R * delta_ij.t;