function [x, sv] = mysvds(A, maxIter)

if nargin < 2
  maxIter=1000;
end

B=A.'*A;
tol=1e-6;
xold=0;
x=rand(length(B),1);
x=x/norm(x);
lastwarn('');
for niter=1:maxIter
  if norm(x-xold)<=tol
    break
  end
  xold=x;
  x = B\x;
  l=1/norm(x);
  x=l*x;
  [dummy wstr]=lastwarn;
  if strcmp(wstr,'MATLAB:nearlySingularMatrix')
    break;
  end
end

if niter == maxIter
  warning('mysvds reached maximum number of iterations')
end
sv = norm(B * x);
