function poseEstimate = vect2Poses3D(poseEstimateVect, nrNodes)

n = nrNodes-1; % number of observable poses

poseEstimateVectTran = poseEstimateVect(1:3*n);
poseEstimateVectRot = poseEstimateVect(3*n+1:end-1);
if length(poseEstimateVectRot) ~= 3*length(poseEstimateVectTran)
  error('wrong size for dual estimate')
end
poseEstimate(1).R = eye(3);
poseEstimate(1).t = zeros(3,1);  
for i=2:nrNodes
  indRow = blockToMatIndices(i-1, 9);
  ri = poseEstimateVectRot(indRow);
  
  poseEstimate(i).R = reshape(ri,3,3)';
  
  if abs(det(poseEstimate(i).R) - 1) > 0.01
    fprintf('Wrong determinant for rotation matrix: %f, **** projecting to SO3 ****\n', det(poseEstimate(i).R))
    % det(poseEstimate(i).R), warning('Wrong determinant for rotation matrix, **** projecting to SO3 ****')
    poseEstimate(i).R = projectToSO3(poseEstimate(i).R);
  end
  
  indRow = blockToMatIndices(i-1, 3);
  ti = poseEstimateVectTran(indRow);
  poseEstimate(i).t = ti;
end