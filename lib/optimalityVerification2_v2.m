function [dhatViaPrimal, normLinSysRes, verify2Time, minEigHlamHat, Hlam] = optimalityVerification2_v2(H_pose, homPoseVectPrimal, n, costPose3DChord)
% COMMENTS:
% same as the version v1, but appied to the SDP with sumLambda+gamma in the objective

disp('====================================================================')
ZZZ = spalloc( 3*n, 3*n, 0); % matrix of zeros
% we assume that the candidate solution is in the form [x 1]
checkEqualWithTol(homPoseVectPrimal(end),1,1e-5);

Hx = H_pose * homPoseVectPrimal; % Hpose(lam) x = 0 <=> Hpose*x + Delta(lam)*x = 0 <=> bx = -Delta(lam)*x 
tranResidue  = Hx(1:3*n);
Hx_rot_y = Hx(3*n+1:end);

%% Build coefficient matrix for lambda
bmTime = tic;
homRotVectPrimal = homPoseVectPrimal(3*n+1:end);  % x_rot_y
A_rot_y = spalloc(9*n+1, 6*n+1, 6*(9*n) + 6*n+1);
Z3 = zeros(3,1);
c = zeros(6*n+1,1);
sel = [1 0 0, 1 0, 1];
for i=1:n
  ind9 = blockToMatIndices(i, 9);
  x1 = homRotVectPrimal(ind9(1:3));
  x2 = homRotVectPrimal(ind9(4:6));
  x3 = homRotVectPrimal(ind9(7:9));

  ind6 = blockToMatIndices(i, 6);
  A_rot_y(ind9,ind6) = [x1 x2 x3 Z3 Z3 Z3;
                        Z3 x1 Z3 x2 x3 Z3;
                        Z3 Z3 x1 Z3 x2 x3];
  c(ind6) = sel;
end
c(6*n+1) = 1; % coefficient of gamma in the objective
A_rot_y(9*n+1, 6*n+1) = +1; % coefficient of gamma
verify2Time.buildLinSysTime = toc(bmTime);

linSolTime = tic;
lam_y = A_rot_y \ Hx_rot_y;
verify2Time.solveLinSysTime = toc(linSolTime);

dhatViaPrimal = c' * lam_y;

%% Compute residul errors, to check if we actually solved the linear system
resTime = tic;
rotResidue = A_rot_y * lam_y - Hx_rot_y;
normTranRes = norm(tranResidue);
normRotRes  = norm(rotResidue);
normLinSysRes = norm([tranResidue; rotResidue]);
verify2Time.computeResidualTime = toc(resTime); 
fprintf('== VERIFICATION2, v2: normTranRes = %f, normRotRes = %f, normLinSysRes = %f \n', normTranRes, normRotRes, normLinSysRes)
if normLinSysRes > 1e-2
  warning('normLinSysRes is NOT zero')
end

%% Check that H(lambda) is not negative definite
lambdaMatHat = spalloc(3*n, 3*n, 3*(3*n));
for i=1:n
  ind6 = blockToMatIndices(i, 6);
  lam_i = lam_y(ind6);
  ind3 = blockToMatIndices(i, 3);
  lambdaMatHat(ind3,ind3) = [lam_i(1) lam_i(2) lam_i(3); lam_i(2) lam_i(4) lam_i(5); lam_i(3) lam_i(5) lam_i(6)];
end
Hlam = H_pose + blkdiag( ZZZ, -kron(lambdaMatHat, eye(3)) , -lam_y(end) );
% Wrong, this is the smallest *magnitude* eigenvalue
%minEigHlamHat = eigs(Hlam, 1, 'SM');
checkEigTime = tic;
minEigHlamHat = computeSmallestEig(Hlam);
verify2Time.computeEigTime = toc(checkEigTime);

% Total time excludes construction of matrices
verify2Time.total = verify2Time.solveLinSysTime + verify2Time.computeResidualTime + verify2Time.computeEigTime;

%  (4) is min([lambdaDiag gamma]): %f >= 0 
fprintf('## VERIFICATION2, v2: (1) is dhat: %.10f == cost: %.10f, (2) is normLinSysRes: %f small?, (3) is mu_min(Hlam): %f >=0, (CPU time: %f)\n', ...
  dhatViaPrimal, costPose3DChord, normLinSysRes, minEigHlamHat, verify2Time.total);

% We can make Hlam positive definite by adding (minEigHlamHat * I). This means subtracting minEigHlamHat to [kron(lambdaMatHat, eye(3)) , lam_y(end)]
% This impacts the objective by minEigHlamHat * (3n+1), since there are 3n
% lambdas in the objective + lam_y(end). Remains to show that the ZZZ part does not matter, Schur complement?
if minEigHlamHat < -1e-4
  fprintf('## VERIFICATION2, EFFECTIVE dhat: dhatEffective: %.10f == cost: %.10f, (CPU time: %f)\n', dhatViaPrimal + minEigHlamHat*(3*n+1), costPose3DChord, verify2Time.total);
end
% full(lambdaMatHat(1:10,1:10))
% trace(lambdaMatHat)-lam_y(end)
% lam_y(end)
% save logVer2