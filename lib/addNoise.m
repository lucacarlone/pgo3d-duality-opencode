function odomPoses = addNoise(odomPoses)

eyePose.t = zeros(3,1);
eyePose.R = eye(3);

for i=1:length(odomPoses)
    [ti,Ri] = poseSubNoisy3D(eyePose, odomPoses(i), 100, 100); % 100 meters, 100rad
    odomPoses(i).R = Ri;
    odomPoses(i).t = ti;
end