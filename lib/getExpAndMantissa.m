function [expo,mant] = getExpAndMantissa(scalar)

if scalar > 1e-12
  expo = floor(log10(scalar));
  mant = scalar/(10^expo);
elseif scalar < -1e-12
  scalar = -scalar;
  expo = floor(log10(scalar));
  mant = - scalar/(10^expo);
else % if it is zero
  expo = 0;
  mant = 0;
end