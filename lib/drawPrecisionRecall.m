function [precision, recall] = drawPrecisionRecall(conditions,rejectedOpt,acceptedSubopt,acceptedOpt)

%% true positives: 
% items correctly labeled as belonging to the positive
% class, i.e., acceptedOpt
%% false positives: 
% items incorrectly labeled as belonging to the positive
% class, i.e., acceptedSubopt
%% false negatives:
% items which were not labeled as belonging to the positive class but should have been
% i.e., rejectedOpt

%% PRECISION: 
% truePositives / (truePositives+falsePositives) = 
precision = acceptedOpt./(acceptedOpt+acceptedSubopt);

%% RECALL:
% truePositives/(truePositives+falseNegatives)=
recall = acceptedOpt./(acceptedOpt+rejectedOpt);

%% ROC:
figure
hold on
plot(conditions,precision,'c','linewidth',2)
plot(conditions,recall,'k','linewidth',2)
title('precision (cyan) - recall (black) curves')

figure
hold on
plot(recall,precision,'m','linewidth',2)
xlabel('recall')
ylabel('precision')