function [nrNodes] = writeTorusDataset3D(nrNodes, coils, probLC, sigmaT, sigmaR, filename) 
% Creates a 3D pose graph optimization with ground truth nodes arranges along a 3D torus 
% - nrNodes: number of nodes in the problem
% - coils: number of coils (trajectory wraps around the torus)
% - probLC: is the probability of loop closures between nearby poses.
% - sigmaT and sigmaR: are the std of the translation and rotation noise
% - filename: name of the file where the pose graph is written (in g2o format)
%
% Author: Luca Carlone
% Georgia Institute of Technology
% Date: May 2014 

% addpath(genpath('../'));
doPlot = 1;
I3 = eye(3);

%% Defult parameters
if nargin < 1
  nrNodes = 5000;
  coils = 100;
  sigmaT = 0.1;
  sigmaR = 0.05;
  probLC = 0.8;
  filename = horzcat('/home/aspn/Desktop/TRO/MOLE2Dmatlab/cleanDatasets/3D/torus3D.g2o');
end

if(doPlot) figure; hold on; end

bigR   = 5;
bigAngles = [linspace(0,2*pi,nrNodes)];
bigCircleCos = cos(bigAngles);
bigCircleSin = sin(bigAngles);

smallR = 1;
coilsAngles = [linspace(0,2*pi,coils)];
smallCircleCos = cos(coilsAngles);
smallCircleSin = sin(coilsAngles);

%% Create ground truth poses
for i=1:nrNodes
  j = rem(i,coils) + 1; % this controls the small circles
  r = bigR + smallR * smallCircleCos(j);
  x = r * bigCircleCos(i);
  y = r * bigCircleSin(i);
  z = smallR * smallCircleSin(j);
  poses(i).t = [x,y,z]';
  if i==1
    poses(i).R = eye(3);
  else
    %th = 2 * pi * rand - pi; 
    %beta = 2 * pi/3 * rand - pi/3; % [-pi/3, pi/3]
    %alpha = pi * rand - pi/2;    % [-pi/2, pi/2]
    %poses(i).R = rotz(rad2deg(th)) * roty(rad2deg(beta)) * rotx(rad2deg(alpha));
    thVect = 2 * pi * (rand(3,1)-0.5); % random vector in the ball of radius pi
    poses(i).R = rot_exp(I3,rot_hat(I3,thVect));
  end
  if(doPlot) 
    plot3( poses(i).t(1), poses(i).t(2), poses(i).t(3),'o'); 
  end
end

%% Create odometric edges
m = nrNodes-1;
for k=1:m
  id1 = k;
  id2 = k+1;
  edges_id(k,1:2) = [id1 id2];
  [tij,Rij] = poseSubNoisy3D(poses(id1), poses(id2), sigmaT, sigmaR);
  measurements.between(k).R = Rij;
  measurements.between(k).t = tij;
  measurements.between(k).Info = [(1/sigmaT^2) * eye(3), zeros(3,3);  
                                      zeros(3,3)       , (1/sigmaR^2) * eye(3) ];
end

%% Create loop closures
for id1=[1:nrNodes]
  if rand<probLC % add loop closure
    t1 = poses(id1).t; % gt positions
    id2 = rem(id1 + coils, nrNodes);
    if id2 == 0
      continue
    end
    t2 = poses(id2).t;
    if(doPlot) plot3( [t1(1) t2(1)], [t1(2) t2(2)], [t1(3) t2(3)],'--'); end
    edge = [id1 id2];
    if findUndirectedEdge(edge, edges_id) == 0
      % add edge if it is not there yet
      m = m+1;
      edges_id(m,1:2) = edge;
      [tij,Rij] = poseSubNoisy3D(poses(id1), poses(id2), sigmaT, sigmaR);
      measurements.between(m).R = Rij;
      measurements.between(m).t = tij;
      measurements.between(m).Info = [(1/sigmaT^2) * eye(3), zeros(3,3);
        zeros(3,3)       , (1/sigmaR^2) * eye(3) ];
    end
  end
end

poseOdom = odometryFromEdges3D(measurements,nrNodes);
writeG2oDataset3D(filename, measurements, edges_id, poseOdom);