function poses = estimatePosesGivenRot(measurements, edges_id, givenRot)
% estimate pose graph configuration given node orientations in pose.R
% it essentially estimate only the positions using linear estimation

m = size(edges_id,1);
nrNodes = max ( max(edges_id(:,1)), max(edges_id(:,2)) );
n = nrNodes - 1;

Delta = [];
for k=1:m
    id = edges_id(k,1);
    p_i = givenRot(id).R * measurements.between(k).t; 
    Delta = [Delta; p_i];
end

[A,At,~] = reducedIncidenceMatrix(edges_id, n, m);
At3 = kron(At,eye(3));
A3 = At3';
pos = (A3 * At3) \ (A3 * Delta);

for i=1:nrNodes
  poses(i).R = givenRot(i).R;
  if i==1
    poses(1).t = [0 0 0]';
  else
    idr = i-1;
    pos_i = pos(3*idr-2 : 3*idr);
    poses(i).t = pos_i(:);
  end
end





