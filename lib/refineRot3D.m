function rotationEstimates = refineRot3D(measurements, rotationEstimates)
% rotationEstimates(i).R contains the i-th rotation
isDebug = 1;

edges_id = measurements.edges_id;
m = size(edges_id,1);
nrNodes = length(rotationEstimates);
J = sparse(spalloc(9*m, nrNodes*3, 6*9*m)); % 6 nonzero per row
fhat = sparse(zeros(9*m,1));
cost = 1e+20;
I3 = eye(3);

%% matrix such that Si * u = ith column of skew(u)
S1 = sparse([0 0 0 ; 0 0 1; 0 -1 0]);
S2 = sparse([0 0 -1; 0 0 0; 1  0 0]);
S3 = sparse([0 1  0;-1 0 0; 0  0 0]);

%% First order expansion of the cost || Ri _Rij - Rj ||_F = 
%  || _Ri (I + S(thi)) _Rij - _Rj (I + S(thj)) ||_F
%  || _Ri _Rij + _Ri S(thi) _Rij - _Rj - _Rj S(thj) ||_F (rearranging)
%  || (_Ri _Rij - _Rj) + _Ri _Rij S(_Rij' thi) - _Rj S(thj) ||_F (vectorizing by columns)
%  || _r + [_Ri _Rij S1; _Ri _Rij S2; _Ri _Rij S3] _Rij' thi - [_Rj S1; _Rj S2; _Rj S3] thj
for iter=1:20
  %% linerizeRot();
  costPrec = cost;
  for k=1:m
    id1 = edges_id(k,1);
    id2 = edges_id(k,2);
    Ri = rotationEstimates(id1).R;
    Rj = rotationEstimates(id2).R;
    Rij = measurements.between(k).R;
    rowInd = blockToMatIndices(k,9);
    Ri_Rij = Ri * Rij;
    fhat(rowInd) = reshape( Ri_Rij - Rj ,9,1);
    
    colInd1 = blockToMatIndices(id1,3);
    J(rowInd,colInd1) = [Ri_Rij*S1 ; Ri_Rij*S2; Ri_Rij*S3] * Rij';
    
    colInd2 = blockToMatIndices(id2,3);
    J(rowInd,colInd2) = -[Rj*S1 ; Rj*S2; Rj*S3];
  end
  %   J( end+1:end+3 , 1:3 ) = I3;
  %   fhat( end+1:end+3 ) = zero(3);

  %% solveRot();
  Jobs = sparse(J(:,4:end)); % we discard anchor
  corrThEst = - (Jobs'*Jobs) \ (Jobs' * fhat); % minus because of fhat
  cost = norm(fhat)^2;
  relCostChange = abs((cost - costPrec)/costPrec);
  if (isDebug) fprintf('Current cost: %f, norm of the correction: %f, relative decrease: %f \n',cost,norm(corrThEst),relCostChange); end
  
  %% updateRot();
  for i=2:nrNodes
    rowInd = blockToMatIndices(i-1,3);
    corrThEst_i = corrThEst(rowInd); % vector 3
    rotationEstimates(i).R = rotationEstimates(i).R * rot_exp(I3,rot_hat(I3,corrThEst_i));
  end
  
  %% check stopping conditions
  if norm(corrThEst) < 1e-3 || relCostChange < 1e-4
    break;
  end
end

end