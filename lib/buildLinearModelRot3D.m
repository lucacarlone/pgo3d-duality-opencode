function [A, b, Afull, Q] = buildLinearModelRot3D(measurements, nrNodes)
% Builds the system matrix Q for the following least squares problem:
%
% min_Ri sum_ij 1/2 ( omega_ij^2 * norm( Ri Rij - Rj , 'fro') ) =
% min_ri^k sum_ij 1/2 omega_ij^2 * sum_{k=1,2,3} || Rij' ri^k - rj^k ||^2 =
% (where ri_k in Real{3} is the k-th row of the rotation Ri) 
%
% = min_ri sum_ij 1/2 omega_ij^2 * || Rijtilde ri - rj ||^2 = 
% (where ri in Real{9} is a vector stacking all rows of Ri) 
%
% = min_r r' * Q * r
%
% Luca Carlone
% Georgia Institute of Technology
% Nov 2014

Z33 = zeros(3,3);
edges_id = measurements.edges_id;
m = length(measurements.between); % num measurements
Afull = spalloc(9*m, 9*nrNodes, 18*9*m); % 18 nonzero elements for each row

for k=1:m 
  Rijt = measurements.between(k).R';
 
  OmegaijR = measurements.between(k).Info(1:3,1:3);
  inf_th = sqrt ( OmegaijR(1,1) ); % inverse of std for rotation measurements
  
  indRow = blockToMatIndices(k, 9);
  
  i = edges_id(k,1);
  indCol = blockToMatIndices(i, 9);
  Afull(indRow, indCol) =  inf_th * [Rijt Z33 Z33; Z33 Rijt Z33; Z33 Z33 Rijt];
  
  j = edges_id(k,2);
  indCol = blockToMatIndices(j, 9);
  Afull(indRow, indCol) = - inf_th * eye(9);
end

Q = Afull' * Afull;

% Ax - b
A = Afull(:,10:end); % fix anchor r1
b = - Afull(:,1:9) * [1 0 0, 0 1 0, 0 0 1]';
