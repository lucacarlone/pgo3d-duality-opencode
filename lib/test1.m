clear all
close all
clc

addpath(genpath('./lib'));
tol = 1e-7;
Ri = rot_exp(eye(3),rot_hat(eye(3),pi*rand(3,1)));
Rj = rot_exp(eye(3),rot_hat(eye(3),pi*rand(3,1)));

% sum(Ri(1,:))^2
% Ri 
% ri = vec(Ri')
% A = zeros(9,9);
% A(1:3,1:3) = ones(3,3);
% ri' * A * ri
% stop

Rij = Ri' * Rj * rot_exp(eye(3),rot_hat(eye(3),0.1*rand(3,1)));

% || Ri Rij - Rj ||
X = Rij' * Ri' - Rj';
Rijt = Rij';
Z33 = zeros(3,3);
ri = [Ri(1,:)'; Ri(2,:)'; Ri(3,:)'];
rj = [Rj(1,:)'; Rj(2,:)'; Rj(3,:)'];
if abs ( norm(X,'fro') - norm( [Rijt Z33 Z33; Z33 Rijt Z33; Z33 Z33 Rijt] * ri - rj ) ) > tol
 error('norms')
end

a1 = norm(X(:,1))^2
if norm(a1 - norm(Rijt *ri(1:3) - rj(1:3))^2) > tol
  error('1')
end
a2 = norm(X(:,2))^2
if norm(a2 - norm(Rijt *ri(4:6) - rj(4:6))^2 ) > tol
  error('2')
end
a3 = norm(X(:,3))^2
if norm(a3 - norm(Rijt *ri(7:9) - rj(7:9))^2 ) > tol
  error('3')
end

disp(' ratio ----')
(a1 + a2) / a3

disp(' sum ----')
a1 + a2 + a3

A = [X(:,1) X(:,2)];
b= X(:,3);
x = (A\b)'
X(:,1)' 
X(:,2)'
X(:,3)'

if norm(x(1) * X(:,1) + x(2) * X(:,2) - X(:,3)) > 1e-8
  error('lin comb')
end

if norm(det(X)) > 1e-8
  error('det')
end

% disp('sing value')
% [e1] = eig(X)
% [e2] = eig(X(:,1:2))
